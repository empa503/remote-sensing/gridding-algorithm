#/!usr/bin/env python
# coding: utf-8

from datetime import datetime
import glob
import os
import multiprocessing
import sys

import matplotlib
matplotlib.use('agg')
import numpy as np
import xarray

import amrs.models
import omi

PATH = "/scratch/snx3000/minsukim/data/Sentinelno2/2018"

DOMAIN_EU = amrs.models.cosmo.Domain('Europe', -12, 30, 42, 72,
                                     ie=54001, je=42001)

DOMAIN_ALPINE = amrs.models.cosmo.Domain('Alpine', 5, 43, 15, 49,
                                         ie=10001, je=6001, pollon=180.0,
                                         pollat=90.0)


def make_figure(grid, time_reference, orbit, domain):
    """
    Make a quick figure for fast reference.
    """
    fig = amrs.vis.make_field_map(grid.lon, grid.lat, 1e6 *
                                  grid['troposphere_mole_content_of_nitrogen_dioxide'].T,
                                  domain=domain, vmin=0, vmax=100,
                                  admin_level=0,
                                  label='NO$_2$ columns (µmol m$^{-2}$)')
    axes = fig.get_axes()
    axes[0].text(0.01, 0.99, '%s (orbit: %d)' % (time_reference, orbit),
                 ha='left', va='top', color='black', transform=axes[0].transAxes)

    return fig


def read_geolocation(filename):

    data = xarray.Dataset()

    group = 'PRODUCT/SUPPORT_DATA/GEOLOCATIONS'
    with xarray.open_dataset(filename, group=group) as nc_file:
        data['lonc'] = nc_file['longitude_bounds'][0].copy()
        data['latc'] = nc_file['latitude_bounds'][0].copy()

    # transpose corners
    data['lonc'] = data['lonc'].transpose('corner', 'scanline', 'ground_pixel')
    data['latc'] = data['latc'].transpose('corner', 'scanline', 'ground_pixel')

    return data


def read_data(filename):
    """
    Read Tropomi NO2 fields
    """
    data = xarray.Dataset()

    with xarray.open_dataset(filename) as nc_file:
        data.attrs['time_reference'] = nc_file.time_reference
        data.attrs['orbit'] = nc_file.orbit

    with xarray.open_dataset(filename, group='PRODUCT') as nc_file:
        data['time_utc'] = nc_file['time_utc'][0].copy().astype('datetime64[ns]')
        data['vcd'] = nc_file['nitrogendioxide_tropospheric_column'][0].copy()
        data['vcd_std'] = nc_file['nitrogendioxide_tropospheric_column_precision'][0].copy()
        data['qa_value'] = nc_file['qa_value'][0].copy()

    return data


def main(filename, output_path, qa_thr=0.5, domain=None, attrs=None):
    """
    Grid Tropomi NO2 fields.
    """
    print(filename)

    # read Level-2 data and create Level-3 grid
    grid = omi.create_grid(domain.startlon, domain.startlat, domain.stoplon,
                           domain.stoplat, domain.dlon, domain.dlat)
    geo_data = read_geolocation(filename)

    # remove data
    mask = omi.mask_grid_domain(grid, geo_data.lonc, geo_data.latc)

    if not np.any(mask):
        return geo_data, grid


    # read observations
    data = read_data(filename)
    data.update(geo_data)

    # clip orbit
    data = omi.clip_orbit(grid, data=data, domain=mask)

    # global attributes
    if attrs is None:
        attrs = {}

    with xarray.open_dataset(filename) as nc_file:
        attrs['title'] = 'TROPOMI/S5P Tropospheric NO2 Level-3 ("Alpine region")'

        attrs['creator_name'] = 'Gerrit Kuhlmann'
        attrs['creator_email'] = 'gerrit.kuhlmann@empa.ch'
        attrs['institution'] = ('Swiss Federal Laboratories for Materials Science '
                                'and Technology (Empa)')

        attrs['data_origin'] = 'TROPOMI/S5P L2 (ID: %s)' % nc_file.attrs['id']
        attrs['references'] = 'OMI Gridding Algorithm (doi: 10.5194/amt-7-451-2014)'

        attrs['date_created'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')

    # Prepare values, errors and weigths
    rho = np.array(data['vcd'])
    errors = np.array(data['vcd_std'])
    weights = np.ones_like(rho)
    missing_values = np.array(data['qa_value'] < qa_thr)

    # grid data
    grid = omi.cvm_grid(grid, np.array(data['lonc']), np.array(data['latc']),
                        rho, errors, weights, missing_values)

    # weight observations
    grid['values'][:] = grid['values'] / grid['weights']

    # update grid field attributes using Level-2 data
    grid.attrs.update(attrs)
    grid['orbit'] = data.attrs['orbit']
    grid['qa_value threshold'] = qa_thr
    grid['values'].attrs.update(data.vcd.attrs)
    grid['errors'].attrs.update(data.vcd_std.attrs)

    # rename values, errors and weights
    name = grid['values'].attrs['standard_name']
    grid[name] = grid['values'].rename(name)
    grid['%s_weights' % name] = grid['weights'].rename('%s_weights' % name)

    name = grid['errors'].attrs['standard_name']
    grid[name] = grid['errors'].rename(name)

    del grid['values']
    del grid['weights']
    del grid['errors']

    # add attributes
    mean_time_utc = data['time_utc'].astype('int64').mean().astype('datetime64[ns]').values
    grid.attrs['mean_time_utc'] = str(mean_time_utc)

    # save to netCDF4 file
    nc_filename = os.path.join(output_path, 'S5P_NO2_%s_o%05d.nc' %
                               (data.time_reference, data.orbit))
    grid.to_netcdf(nc_filename)

    # make image showing NO2 field
    fig = make_figure(grid, mean_time_utc, grid.orbit, domain)
    image_filename = os.path.join(output_path, 'S5P_NO2_%s_o%05d.png' %
                                  (data.time_reference, data.orbit))
    fig.savefig(image_filename)

    #return data, grid



if __name__ == '__main__':

    #number, n_cpu = int(sys.argv[1]), int(sys.argv[2])

    filenames = sorted(glob.glob(os.path.join(PATH, '*.nc')))#[number::n_cpu]
    output_path = '/scratch/snx3000/gkuhl/S5P'

    with multiprocessing.Pool(processes=15) as pool:
        pool.starmap(main, [(fn, output_path, 0.5, DOMAIN_ALPINE) for fn in
                            filenames])

    #for fn in filenames:
    #    main(fn, output_path, 0.5, DOMAIN_ALPINE)

