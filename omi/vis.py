#! /usr/bin/env python
# coding: utf-8

from __future__ import division, print_function

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp

from mpl_toolkits.basemap import Basemap


def draw_grid(grid, vmin=0, vmax=1e16, resolution='i'):

    fig, axes = plt.subplots(1,1)
    m = Basemap(ax=axes, resolution=resolution, **grid.to_basemap())

    m.drawcountries(color='white')
    m.drawcoastlines(color='white')
    m.fillcontinents(color='gray', lake_color='silver', zorder=0)
    m.drawmapboundary(color='silver', fill_color='silver')

    m.imshow(
        grid.values.T, vmin=vmin, vmax=vmax, cmap='jet',
        interpolation='nearest', origin='lower'
    )





if __name__ == '__main__':
    pass
